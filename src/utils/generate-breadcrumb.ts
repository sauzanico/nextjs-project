export function generateBreadcrumb(path: string[]) {
  path.shift();
  const breadcrumbList = path.map((item, index) => {
    const href = path.slice(0, index + 1).join('/');
    return {
      name: capitalizeSlug(item) || 'Dashboard',
      href: `/${href}`,
    };
  });

  return breadcrumbList;
}

export function capitalizeSlug(slug: string): string | undefined {
  if (!slug) {
    return undefined;
  }
  const words = slug.split('-');

  for (let i = 0; i < words.length; i++) {
    const word = words[i];
    words[i] = word.charAt(0).toUpperCase() + word.slice(1);
  }

  return words.join(' ');
}
