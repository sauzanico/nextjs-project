export type Query = Record<
  string,
  string | string[] | number | null | undefined
>;

export function cleanQuery(query: Query) {
  return Object.keys(query).reduce(
    (cleanedQuery, queryKey) =>
      query[queryKey]
        ? { ...cleanedQuery, [queryKey]: query[queryKey] }
        : { ...cleanedQuery },
    {} as Query,
  );
}
