import React from 'react';
import { Text, Box } from '@chakra-ui/react';
import { Layout } from '@components';

function NotFound() {
  return (
    <Layout>
      <Box mt={12}>
        <Text
          fontFamily="poppins"
          fontSize="160px"
          lineHeight="none"
          fontWeight="semibold"
          textAlign="center"
          color="tosca.100"
        >
          404
        </Text>
        <Text
          fontSize="40px"
          lineHeight="none"
          fontWeight="semibold"
          textAlign="center"
        >
          Page not found
        </Text>
        <Text fontSize="xl" textAlign="center" mt={10} mb={10}>
          Sorry, the page you&apos;re looking for cannot be accessed. <br />{' '}
          Here are few links that may be helpful:
        </Text>
      </Box>
    </Layout>
  );
}

export default NotFound;
