import React from 'react';

import { Box, Text, HStack, VStack, Container, Image } from '@chakra-ui/react';
import { LoginCard, Header } from '../../components';

export default function LoginPage() {
  return (
    <Box bg="primaryGray">
      <Header />
      <Container width="1200px" maxWidth="1200px" paddingInline={0}>
        <HStack h="calc(100vh - 120px)">
          <HStack alignItems="start" justifyContent="space-between">
            <Box w="450px">
              <Image src="/login-kereta.png" alt="kereta" />
            </Box>
            <VStack justifyContent="center" w="calc(100% - 500px)" spacing={8}>
              <Box bg="white" rounded="xl" pt={8} pb={12} px={12}>
                <Text
                  fontSize="40px"
                  color="primaryGreen"
                  fontWeight="bold"
                  textAlign="center"
                >
                  Selamat Datang
                </Text>
                <Text fontSize="lg" mt={3} textAlign="justify">
                  Sistem Pemantauan Kinerja Operasi Kereta Api merupakan sistem
                  yang digunakan untuk mendaftarkan Sarana Kereta Api yang
                  melintas pada Prasana Perkertaapian milik Negara
                </Text>
              </Box>
              <LoginCard />
            </VStack>
          </HStack>
        </HStack>
      </Container>
    </Box>
  );
}
