import React, { ReactElement } from 'react';
import { Button } from '@chakra-ui/react';
import { Layout, LogoutIcon } from '@components';

export default function BadanUsaha() {
  return (
    // <Layout activeMenu="Component">
    <>
      <LogoutIcon boxSize={5} />
      <Button variant="outline">Sarana</Button>
    </>
    // </Layout>
  );
}

BadanUsaha.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
