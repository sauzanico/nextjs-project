import React, { ReactElement } from 'react';
import { Button, Text } from '@chakra-ui/react';
import { DataSarana } from '@features';
import { Layout, LogoutIcon } from '@components';

export default function Add() {
  return (
    <>
      <Text>Halaman Tambah Sarana</Text>
    </>
  );
}

Add.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
