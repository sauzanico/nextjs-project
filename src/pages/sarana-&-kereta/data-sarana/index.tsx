import React, { ReactElement } from 'react';
import { Button } from '@chakra-ui/react';
import { DataSarana } from '@features';
import { Layout, LogoutIcon } from '@components';

export default function DataSaranaPage() {
  return <DataSarana />;
}

DataSaranaPage.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
