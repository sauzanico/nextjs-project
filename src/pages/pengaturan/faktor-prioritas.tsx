import React, { ReactElement } from 'react';
import { Button } from '@chakra-ui/react';
import { Layout, LogoutIcon } from '@components';

export default function FaktorPrioritas() {
  return (
    // <Layout activeMenu="Component">
    <>
      <LogoutIcon boxSize={5} />
      <Button variant="outline">Sarana</Button>
    </>
    // </Layout>
  );
}

FaktorPrioritas.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
