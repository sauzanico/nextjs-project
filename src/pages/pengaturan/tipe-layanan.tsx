import React, { ReactElement } from 'react';
import { Button } from '@chakra-ui/react';
import { Layout, LogoutIcon } from '@components';

export default function TipeLayanan() {
  return (
    // <Layout activeMenu="Component">
    <>
      <LogoutIcon boxSize={5} />
      <Button variant="outline">Sarana</Button>
    </>
    // </Layout>
  );
}

TipeLayanan.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
