import React, { ReactElement } from 'react';
import { Dashboard } from '@features';
import { Layout } from '@components';

export default function Home() {
  return <Dashboard />;
}

Home.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
