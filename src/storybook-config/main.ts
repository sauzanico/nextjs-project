import type { StorybookConfig } from '@storybook/react/types';

const config: StorybookConfig = {
  stories: ['../**/*.stories.mdx', '../**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@chakra-ui/storybook-addon',
  ],
  framework: '@storybook/react',
  core: {
    builder: {
      name: 'webpack5',
      options: {
        lazyCompilation: true,
        fsCache: true,
      },
    },
  },
  features: {
    postcss: false,
    emotionAlias: false,
    buildStoriesJson: true,
    warnOnLegacyHierarchySeparator: false,
  },
};

module.exports = config;
