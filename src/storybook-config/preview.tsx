import React, { FunctionComponent } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { theme } from '../components/theme';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 0,
      cacheTime: 0,
      retry: false,
    },
  },
});

export const parameters = {
  chakra: {
    theme: theme,
  },
};

function appDecorator(StoryFn: FunctionComponent) {
  return (
    <QueryClientProvider client={queryClient}>
      <StoryFn />
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export const decorators = [appDecorator];
