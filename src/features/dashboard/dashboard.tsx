import React, { ReactElement } from 'react';
import { Button, Box, Link, VStack, HStack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { Layout, SimpleTable, PaginationLink, LoginCard } from '@components';
import { cleanQuery } from '@utils';
import { DataSaranaFilter } from './data-sarana-filter';

export function Dashboard() {
  const Router = useRouter();
  return (
    <Box w="full">
      <HStack w="full">
        <Text fontWeight="bold" fontSize="4xl">
          Dashboard
        </Text>
      </HStack>
      <Box my={4}>
        <DataSaranaFilter />
      </Box>
      <SimpleTable />
      <PaginationLink
        page={+`${1}`}
        maxPage={10}
        pathname="/"
        total={`${100} Unit`}
        currentQuery={{
          ...(cleanQuery(Router.query) as Record<string, string>),
        }}
      />
    </Box>
  );
}
