import React, { ReactElement } from 'react';
import { Button, Box, Link, VStack, HStack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { Layout, SimpleTable, PaginationLink, LoginCard } from '@components';
import { cleanQuery } from '@utils';
import { DataSaranaFilter } from './data-sarana-filter';
import { DataSaranaAddButton } from './data-sarana-add-button';

export function DataSarana() {
  const Router = useRouter();
  const currentQuery = cleanQuery(Router.query) as Record<string, string>;
  return (
    <VStack w="full" spacing={8}>
      <HStack w="full" justifyContent="space-between">
        <Text fontWeight="bold" fontSize="4xl">
          Data Sarana
        </Text>
        <DataSaranaAddButton />
      </HStack>
      <DataSaranaFilter />
      <SimpleTable />
      <PaginationLink
        page={+`${currentQuery.page}`}
        maxPage={10}
        pathname="/sarana-&-kereta/data-sarana"
        total={`${80} Unit`}
        currentQuery={{ ...currentQuery }}
      />
    </VStack>
  );
}
