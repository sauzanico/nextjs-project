import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { object, string } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { usePostLogin } from './login-queries';

type FilterVariables = {
  namaSarana: string;
  jenisSarana: string;
};

type FilterForm = {
  namaSarana: Yup.StringSchema<FilterVariables['namaSarana']>;
  jenisSarana: Yup.StringSchema<FilterVariables['jenisSarana']>;
};

const loginFieldValidation: FilterForm = {
  namaSarana: string().label('namaSarana').trim().required().min(3).max(64),
  jenisSarana: string().label('jenisSarana').trim().required().min(3).max(64),
};

export const filterFormSchema = object().shape(loginFieldValidation);

export function useFilterForm() {
  const {
    register,
    handleSubmit,
    watch,
    control,
    formState: { errors },
    setValue,
  } = useForm({ resolver: yupResolver(filterFormSchema), mode: 'onChange' });

  const { mutate, isLoading } = usePostLogin();

  const onSubmit = (data: FilterVariables) => {
    const { namaSarana, jenisSarana } = data;
    console.log(data);
    // mutate(
    //   { namaSarana, jenisSarana },
    //   {
    //     onSuccess: () => {
    //       console.log('suces');
    //     },
    //     onError: ({ message }) => {
    //       console.log(message);
    //     },
    //   },
    // );
  };

  const submitLogin = handleSubmit((e) => onSubmit(e as FilterVariables));

  return {
    register,
    watch,
    handleSubmit,
    submitLogin,
    isLoading,
    control,
    errors,
    setValue,
  };
}
