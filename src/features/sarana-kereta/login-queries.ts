import { useMutation } from 'react-query';
import axios from 'axios';

export type LoginParams = {
  username: string;
  password: string;
};

export async function login(params: LoginParams) {
  const url = 'https://reqres.in/api/login';
  const res = await axios.post(url, params);
  return res;
}

export type PostLoginCache = Awaited<ReturnType<typeof login>>;

export function usePostLogin() {
  return useMutation<PostLoginCache, any, LoginParams>(login);
}
