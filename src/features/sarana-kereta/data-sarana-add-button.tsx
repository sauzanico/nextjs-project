import React from 'react';
import { Button } from '@chakra-ui/react';
import { Link } from '@components/ui-kit';

export function DataSaranaAddButton() {
  return (
    <>
      <Link
        href={{
          pathname: '/sarana-&-kereta/data-sarana/add',
        }}
      >
        <Button>+ Tambah</Button>
      </Link>
    </>
  );
}
