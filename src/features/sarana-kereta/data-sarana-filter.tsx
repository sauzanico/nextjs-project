import React, { useState } from 'react';
import { Button, HStack } from '@chakra-ui/react';
import { Box, VStack } from '@chakra-ui/react';
import { TextInput, Dropdown, Search } from '@components';
import { useFilterForm } from './data-sarana-filter-form';

export function DataSaranaFilter() {
  const { control, errors, submitLogin, isLoading, setValue } = useFilterForm();
  const [dropdownData, setDropdownData] = useState(null);
  const options = [
    {
      label: 'test',
      value: 'test',
    },
    {
      label: 'bebas',
      value: 'bebas',
    },
  ];
  const onChange = (e: any) => {
    setDropdownData(e);
  };
  return (
    <HStack p={4} alignItems="start" shadow="simple" w="full">
      <TextInput
        name="namaSarana"
        type="text"
        control={control}
        errors={errors.namaSarana}
        placeholder="Cari nomor sarana"
        label="Nomor Sarana"
        rightElement={<Search />}
      />
      <Dropdown
        name="jenisSarana"
        control={control}
        options={options}
        errors={errors.jenisSarana}
        onChange={onChange}
        value={dropdownData}
        placeholder="Semua jenis sarana"
        label="Jenis Sarana"
        setValue={setValue}
      />

      <Box textAlign="end" w="full" mt={4}>
        <Button
          rightIcon={<Search />}
          type="submit"
          onClick={submitLogin}
          isLoading={isLoading}
        >
          Cari
        </Button>
      </Box>
    </HStack>
  );
}
