import React, { PropsWithChildren } from 'react';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';
import {
  Link as ChakraLink,
  LinkProps as ChakraLinkProps,
  LinkOverlay as ChakraLinkOverlay,
  LinkOverlayProps as ChakraLinkOverlayProps,
} from '@chakra-ui/react';

export type LinkProps = PropsWithChildren<
  NextLinkProps & Omit<ChakraLinkProps, 'as' | 'href'>
>;

export function Link({
  as,
  href,
  replace,
  scroll,
  shallow,
  prefetch,
  children,
  ...chakraProps
}: LinkProps) {
  return (
    <NextLink
      as={as}
      href={href}
      passHref={true}
      scroll={scroll}
      replace={replace}
      shallow={shallow}
      prefetch={prefetch}
    >
      <ChakraLink as="span" {...chakraProps}>
        {children}
      </ChakraLink>
    </NextLink>
  );
}

export type LinkOverlayProps = PropsWithChildren<
  NextLinkProps & Omit<ChakraLinkOverlayProps, 'as'>
>;

export function LinkOverlay({
  as,
  href,
  replace,
  scroll,
  shallow,
  prefetch,
  children,
  ...chakraProps
}: LinkProps) {
  return (
    <NextLink
      as={as}
      href={href}
      passHref={true}
      scroll={scroll}
      replace={replace}
      shallow={shallow}
      prefetch={prefetch}
    >
      <ChakraLinkOverlay {...chakraProps}>{children}</ChakraLinkOverlay>
    </NextLink>
  );
}
