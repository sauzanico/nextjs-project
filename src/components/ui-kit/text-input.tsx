import React, { forwardRef, ReactElement, useRef } from 'react';
import {
  Input,
  FormControl,
  FormLabel,
  FormErrorMessage,
  useDisclosure,
  IconButton,
  InputGroup,
  InputProps,
  InputRightElement,
  useMergeRefs,
} from '@chakra-ui/react';
import {
  Controller,
  FieldValues,
  Control,
  FieldError,
  FieldErrorsImpl,
  Merge,
} from 'react-hook-form';
import { EyeOpen, EyeClose } from '@components/icons';

export type Props = {
  control: Control<FieldValues, any>;
  name: string;
  type: string;
  label: string;
  placeholder: string;
  errors?: FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
  rightElement?: ReactElement;
};

export function TextInput({
  control,
  name,
  type,
  errors,
  label,
  placeholder,
  rightElement,
}: Props) {
  const color = 'primary.40';

  return (
    <FormControl isInvalid={!!errors}>
      <FormLabel htmlFor={name} color={color}>
        {label}
      </FormLabel>
      <Controller
        control={control}
        name={name}
        render={({ field }) => {
          if (type === 'password') {
            return (
              <PasswordInput
                {...field}
                id={name}
                color={color}
                borderWidth={1}
                borderColor="transparent"
                _invalid={{ borderColor: 'red.400' }}
              />
            );
          }
          return (
            <>
              <InputGroup>
                {rightElement && (
                  <InputRightElement mx={2}>{rightElement}</InputRightElement>
                )}
                <Input
                  {...field}
                  id={name}
                  type={type}
                  placeholder={placeholder}
                  color={color}
                  borderWidth={1}
                  borderColor={color}
                  _invalid={{ borderColor: 'red.400' }}
                />
              </InputGroup>
            </>
          );
        }}
      />
      <FormErrorMessage color="red.400">
        {errors?.message as string}
      </FormErrorMessage>
    </FormControl>
  );
}

export const PasswordInput = forwardRef<HTMLInputElement, InputProps>(
  (props, ref) => {
    const { isOpen, onToggle } = useDisclosure();
    const inputRef = useRef<HTMLInputElement>(null);

    const mergeRef = useMergeRefs(inputRef, ref);
    const onClickReveal = () => {
      onToggle();
      if (inputRef.current) {
        inputRef.current.focus({ preventScroll: true });
      }
    };

    return (
      <InputGroup>
        <InputRightElement>
          <IconButton
            size="sm"
            bg="transparent"
            _hover={{
              bg: 'transparent',
            }}
            aria-label={isOpen ? 'Mask password' : 'Reveal password'}
            icon={isOpen ? <EyeOpen boxSize={5} /> : <EyeClose boxSize={5} />}
            onClick={onClickReveal}
            mr={3}
          />
        </InputRightElement>
        <Input
          ref={mergeRef}
          type={isOpen ? 'text' : 'password'}
          autoComplete="current-password"
          {...props}
        />
      </InputGroup>
    );
  },
);

PasswordInput.displayName = 'PasswordInput';
