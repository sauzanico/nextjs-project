import React from 'react';
import { Meta, StoryObj } from '@storybook/react';

import { Button, ButtonProps } from '../button';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'UI KIT / Buttons',
  component: Button,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta;

export const Primary: StoryObj<ButtonProps> = {
  render: (args) => <Button {...args}>label</Button>,
  args: {
    variant: 'outline',
    type: 'primary',
    isRounded: true,
  },
};

export const Secondary: StoryObj<ButtonProps> = {
  render: (args) => <Button {...args}>label</Button>,
  args: {
    variant: 'solid',
    type: 'primary',
  },
};
