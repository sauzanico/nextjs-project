import React, { ReactElement } from 'react';
import {
  Button as ChakraButton,
  ButtonProps as ChakraButtonProps,
} from '@chakra-ui/react';

export type ButtonProps = {
  variant: 'solid' | 'outline';
  type: 'primary' | 'secondary' | 'tersier';
  children: ReactElement | string;
  onClick?: () => void;
  isRounded?: boolean;
};

export const Button = ({
  variant,
  children,
  type,
  isRounded = false,
}: ButtonProps) => {
  const baseColor =
    type === 'primary'
      ? 'primary.100'
      : type === 'secondary'
      ? 'secondary.100'
      : 'tersier';
  const isSolid = variant === 'solid';

  const solid = {
    bgColor: baseColor,
    color: 'white',
  };

  const outline: ChakraButtonProps = {
    borderColor: baseColor,
    color: baseColor,
    borderWidth: '1px',
  };

  const newProps: ChakraButtonProps = {
    borderRadius: isRounded ? 'lg' : 'sm',
    ...(isSolid ? solid : outline),
  };

  return <ChakraButton {...newProps}>{children}</ChakraButton>;
};
