import React from 'react';
import {
  Skeleton,
  SkeletonText,
  VStack,
  HStack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Badge,
  Text,
} from '@chakra-ui/react';
import { Atom } from '@components/icons';

export function SimpleTable() {
  const heads = [
    { name: 'NO. SARANA', orderAble: true },
    { name: 'JENIS SARANA', orderAble: false },
    { name: 'TIPE SARANA', orderAble: false },
    { name: 'JENIS PENGGERAK', orderAble: false },
    { name: 'BERAT KOSONG', orderAble: false },
    { name: 'BERAT ISI', orderAble: false },
    { name: 'AKSI', orderAble: false, hasAction: true },
  ];
  const data = [
    [
      { name: 'NO. SARANA', value: <Badge>badgeee</Badge> },
      { name: 'JENIS SARANA', value: 'Dummy' },
      { name: 'TIPE SARANA', value: 'Dummy' },
      { name: 'JENIS PENGGERAK', value: 'Dummy' },
      { name: 'BERAT KOSONG', value: 'Dummy' },
      { name: 'BERAT ISI', value: 'Dummy' },
      {
        name: 'AKSI',
        value: 'Lihat Detail',
      },
    ],
    [
      { name: 'NO. SARANA', value: <Badge>badgeee</Badge> },
      { name: 'JENIS SARANA', value: 'Dummy' },
      { name: 'TIPE SARANA', value: 'Dummy' },
      { name: 'JENIS PENGGERAK', value: 'Dummy' },
      { name: 'BERAT KOSONG', value: 'Dummy' },
      { name: 'BERAT ISI', value: 'Dummy' },
      {
        name: 'AKSI',
        value: 'Lihat Detail',
      },
    ],
  ];
  return (
    <TableContainer
      rounded="md"
      border="1px solid"
      borderColor="neutral.20"
      w="full"
    >
      <Table>
        <Thead>
          <Tr bg="primary.10">
            {heads.map((head) => {
              return (
                <Th
                  key={head.name}
                  textAlign="center"
                  borderBottom="1px solid"
                  borderColor="neutral.20"
                >
                  <HStack spacing={3}>
                    <Text>{head.name}</Text>
                    {head.orderAble && <Atom boxSize={3} />}
                  </HStack>
                </Th>
              );
            })}
          </Tr>
        </Thead>
        <Tbody>
          {data.map((item, index) => {
            return (
              <Tr key={index}>
                {item.map((it, idx) => {
                  return (
                    <Td
                      key={idx}
                      borderBottom="1px solid"
                      borderColor="neutral.20"
                    >
                      {it.value}
                    </Td>
                  );
                })}
              </Tr>
            );
          })}
          {/* {[1, 2].map((id) => (
            <Tr key={id}>
              <Td width="220px">
                <VStack align="flex-start" spacing={2} w="full" m="0px 10px">
                  <SkeletonText
                    w="60%"
                    noOfLines={1}
                    spacing={1}
                    skeletonHeight={2.5}
                  />
                  <SkeletonText
                    w="40%"
                    noOfLines={1}
                    spacing={1}
                    skeletonHeight={2.5}
                  />
                </VStack>
              </Td>
              <Td width="300px">
                <Skeleton w="220px" h="80px" borderRadius="xl" m="auto" />
              </Td>
              <Td>
                <VStack align="flex-start" w="80%">
                  <SkeletonText w="90%" noOfLines={1} />
                  <SkeletonText w="30%" noOfLines={1} />
                  <SkeletonText w="80%" noOfLines={1} />
                  <SkeletonText w="30%" noOfLines={1} />
                  <SkeletonText w="60%" noOfLines={1} />
                  <SkeletonText pl={4} w="30%" noOfLines={3} />
                  <SkeletonText w="50%" noOfLines={1} />
                  <SkeletonText pl={4} w="30%" noOfLines={3} />
                </VStack>
              </Td>
              <Td w="160px">
                <Badge>badgeee</Badge>
              </Td>
            </Tr>
          ))} */}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
