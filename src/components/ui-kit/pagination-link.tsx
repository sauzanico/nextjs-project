import React from 'react';
import { HStack, Box, Text } from '@chakra-ui/react';
import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import { Link } from './link';
// import Link from 'next/link';

export type PaginationLinkProps = {
  total?: string;
  page: number;
  maxPage: number;
  pathname: string;
  currentQuery?: Record<string, string | string[]>;
};

const paginationLinkStyle = {
  borderRadius: 'base',
  height: '32px',
  width: '32px',
  fontWeight: 'semibold',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
};

export function PaginationLink(props: PaginationLinkProps) {
  const { page, maxPage, pathname, currentQuery, total } = props;
  const showPage = generatePageNumber(page, maxPage);
  const otherProps = {
    page,
    maxPage,
    showPage,
    pathname,
    currentQuery,
  };
  return (
    <HStack justifyContent="space-between" w="full">
      {total && <Text>Total: {total}</Text>}
      {maxPage > 1 && (
        <HStack spacing={1}>
          <GeneratePrevLink {...otherProps} />
          <GeneratePagination {...otherProps} />
          <GenerateNextLink {...otherProps} />
        </HStack>
      )}
    </HStack>
  );
}

export type GeneratePaginationProps = PaginationLinkProps & {
  showPage: { value: number; text: string }[];
};

function GeneratePagination(props: GeneratePaginationProps) {
  const { page, showPage, pathname, currentQuery } = props;
  return (
    <HStack spacing={1}>
      {showPage.map((pages: { value: number; text: string }) => (
        <Link
          scroll
          shallow
          href={{
            pathname,
            query: {
              ...currentQuery,
              page: pages.text,
            },
          }}
          key={`${pages.text}-${pages.value}`}
          {...paginationLinkStyle}
          {...(pages.value == page
            ? { color: 'white', background: 'primary.60' }
            : { color: 'black' })}
        >
          {pages.text}
        </Link>
      ))}
    </HStack>
  );
}

function generatePageNumber(page: number, maxPage: number) {
  const showPage = Array.from(Array(maxPage < 7 ? maxPage : 7), (e, i) =>
    Object.assign({ value: i + 1, text: (i + 1).toString() }),
  );
  if (maxPage > 7) {
    showPage[5] = { value: page + 1, text: '...' };
    showPage[6] = { value: maxPage, text: maxPage.toString() };

    if (page > 3) {
      showPage[1] = { value: page - 1, text: '...' };
      showPage[2] = { value: page - 1, text: (page - 1).toString() };
      showPage[3] = { value: page, text: page.toString() };
      showPage[4] = { value: page + 1, text: (page + 1).toString() };
    }

    if ([maxPage, maxPage - 1].includes(page)) {
      showPage[2] = { value: maxPage - 4, text: (maxPage - 4).toString() };
      showPage[3] = { value: maxPage - 3, text: (maxPage - 3).toString() };
      showPage[4] = { value: maxPage - 2, text: (maxPage - 2).toString() };
      showPage[5] = { value: maxPage - 1, text: (maxPage - 1).toString() };
    }
  }

  return showPage;
}

function GeneratePrevLink(props: PaginationLinkProps) {
  const { page, pathname, currentQuery } = props;
  if (page <= 1) {
    return (
      <Box color="neutral.30" {...paginationLinkStyle}>
        <ChevronLeftIcon w={6} h={6} />
      </Box>
    );
  }
  return (
    <>
      <Link
        scroll
        shallow
        href={{
          pathname,
          query: {
            ...currentQuery,
            page: page - 1,
          },
        }}
        key="prev-page"
        color="black"
        {...paginationLinkStyle}
      >
        <ChevronLeftIcon w={6} h={6} />
      </Link>
    </>
  );
}

function GenerateNextLink(props: PaginationLinkProps) {
  const { page, pathname, maxPage, currentQuery } = props;
  if (page >= maxPage) {
    return (
      <Box color="neutral.30" {...paginationLinkStyle}>
        <ChevronRightIcon w={6} h={6} />
      </Box>
    );
  }
  return (
    <>
      <Link
        scroll
        shallow
        href={{
          pathname,
          query: {
            ...currentQuery,
            page: page + 1,
          },
        }}
        key="next-page"
        color="black"
        {...paginationLinkStyle}
      >
        <ChevronRightIcon w={6} h={6} />
      </Link>
    </>
  );
}
