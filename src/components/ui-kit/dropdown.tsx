import React from 'react';
import {
  VisuallyHiddenInput,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,
  ButtonProps,
  FormLabel,
  Box,
} from '@chakra-ui/react';
import { ChevronDownIcon } from '@chakra-ui/icons';
import {
  Controller,
  FieldValues,
  Control,
  FieldError,
  FieldErrorsImpl,
  Merge,
  UseFormSetValue,
} from 'react-hook-form';
import { Train } from '@components/icons';

type DefaultDropdownOption = { value: string; label: string };

type DropdownValue<Option> = Option extends DefaultDropdownOption
  ? Option['value']
  : never;

export type DropdownProps<
  Options extends DefaultDropdownOption[] = DefaultDropdownOption[],
> = {
  options: Options;
  onChange: (newOption: DropdownValue<Options[number]>) => void;
  value?: DropdownValue<Options[number]> | '' | null;
  isMobile?: boolean;
  isError?: boolean;
  placeholder?: string;
  inputProps?: ButtonProps;
  label?: string;
  name: string;
  errors?: FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
  control: Control<FieldValues, any>;
  setValue: UseFormSetValue<FieldValues>;
} & ({ isMobile?: false } | { isMobile: true; title?: string });

export function Dropdown<Options extends DefaultDropdownOption[]>(
  props: DropdownProps<Options>,
) {
  const {
    options,
    value,
    onChange,
    isError,
    placeholder,
    inputProps,
    label,
    control,
    name,
    errors,
    setValue,
  } = props;
  const selectedOption = options.find((option) => option.value == value);
  const color = 'primary.40';

  return (
    <Box w="full">
      <FormLabel color={color}>{label}</FormLabel>
      <Controller
        control={control}
        name={name}
        render={({ field }) => {
          return <VisuallyHiddenInput {...field} />;
        }}
      />

      <Menu>
        <MenuButton
          as={Button}
          w="full"
          variant="outline"
          borderWidth={1}
          borderColor={color}
          bg="neutral.10"
          fontWeight="normal"
          textAlign="left"
          _hover={{
            bg: 'neutral.10',
          }}
          _active={{
            bg: 'neutral.10',
          }}
          color={selectedOption ? 'primary.90' : color}
          leftIcon={<Train boxSize={5} color={color} />}
          rightIcon={
            <ChevronDownIcon boxSize={5} color={isError ? 'red.500' : color} />
          }
          {...(selectedOption
            ? {
                value: selectedOption.value,
              }
            : null)}
          {...inputProps}
        >
          {selectedOption?.label ?? placeholder}
        </MenuButton>
        <MenuList zIndex="popover">
          {options.map((option) => (
            <MenuItem
              key={option.value}
              value={option.value}
              _hover={{
                background: 'veryLightPink',
              }}
              onClick={(e) => {
                e.preventDefault();
                setValue(name, option.value, {
                  shouldValidate: true,
                  shouldDirty: true,
                });
                onChange(option.value as DropdownValue<Options[number]>);
              }}
            >
              {option.label}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
      <Box color="red.400">{errors?.message as string}</Box>
    </Box>
  );
}
