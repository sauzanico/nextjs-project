import React from 'react';
import { Meta } from '@storybook/react';

import { LoginCard } from '..';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'UI KIT / LoginCard',
  component: LoginCard,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta;

export const Desktop = {
  render: (args: any) => <LoginCard {...args} />,
  args: {
    variant: 'outline',
    type: 'primary',
    isRounded: true,
  },
};

// export const Secondary: StoryObj<ButtonProps> = {
//   render: args => <Button {...args}>label</Button>,
//   args: {
//     variant: 'solid',
//     type: 'primary',
//   },
// };
