import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { object, string } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { usePostLogin } from './login-queries';

type LoginVariables = {
  username: string;
  password: string;
};

type LoginForm = {
  username: Yup.StringSchema<LoginVariables['username']>;
  password: Yup.StringSchema<LoginVariables['password']>;
};

const loginFieldValidation: LoginForm = {
  username: string().label('username').trim().required().min(3).max(64),
  password: string().label('password').trim().required().min(3).max(64),
};

export const loginFormSchema = object().shape(loginFieldValidation);

export function useLoginForm() {
  const {
    register,
    handleSubmit,
    watch,
    control,
    formState: { errors },
  } = useForm({ resolver: yupResolver(loginFormSchema), mode: 'onChange' });

  const { mutate, isLoading } = usePostLogin();

  const onSubmit = (data: LoginVariables) => {
    const { username, password } = data;
    mutate(
      { username, password },
      {
        onSuccess: () => {
          console.log('suces');
        },
        onError: ({ message }) => {
          console.log(message);
        },
      },
    );
  };

  const submitLogin = handleSubmit((e) => onSubmit(e as LoginVariables));

  return {
    register,
    watch,
    handleSubmit,
    submitLogin,
    isLoading,
    control,
    errors,
  };
}
