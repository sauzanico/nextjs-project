import React, { useState } from 'react';
import {
  Button,
  Box,
  Text,
  FormControl,
  FormLabel,
  FormErrorMessage,
  VStack,
} from '@chakra-ui/react';
import { Search } from '@components/icons';
import { TextInput, Dropdown } from '../ui-kit';
import { useLoginForm } from './login-form';

export function LoginCard() {
  const { control, errors, submitLogin, isLoading } = useLoginForm();
  const [dropdownData, setDropdownData] = useState(null);
  const options = [
    {
      label: 'test',
      value: 'test',
    },
    {
      label: 'bebas',
      value: 'bebas',
    },
  ];
  return (
    <Box bg="primaryGreen" rounded="lg" w="500px" p={4} pb={8}>
      <Text fontSize="xl" textAlign="center">
        LOGIN
      </Text>
      <VStack background="paleBlue.500">
        {/* <TextInput
          name="username"
          type="text"
          control={control}
          errors={errors.username}
          label="Username"
          rightElement={<Search />}
        />
        <TextInput
          name="password"
          type="password"
          control={control}
          errors={errors.password}
          label="Password"
        />
        <Dropdown
          options={options}
          onChange={onChange}
          isMobile={false}
          value={dropdownData}
          placeholder="Semua"
        /> */}

        <Box textAlign="end" w="full" mt={4}>
          <Button
            colorScheme="neutral"
            type="submit"
            onClick={submitLogin}
            isLoading={isLoading}
          >
            Submit
          </Button>
        </Box>
      </VStack>
    </Box>
  );
}
