import { ThemeConfig } from '@chakra-ui/react';

export const config: ThemeConfig = {
  cssVarPrefix: 'spn',
  initialColorMode: 'light',
  useSystemColorMode: false,
};
