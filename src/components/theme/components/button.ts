import type {
  SystemStyleObject,
  SystemStyleFunction,
} from '@chakra-ui/theme-tools';

const baseStyle: SystemStyleObject = {
  lineHeight: '1.2',
  borderRadius: 'md',
  fontFamily: 'poppins',
  fontWeight: 'semibold',
  transitionProperty: 'common',
  transitionDuration: 'normal',
  _focus: {
    boxShadow: 'outline',
  },
  _disabled: {
    opacity: 0.4,
    cursor: 'not-allowed',
    boxShadow: 'none',
  },
  _hover: {
    _disabled: {
      bg: 'initial',
    },
  },
};

const variantGhost: SystemStyleFunction = (props) => {
  const { colorScheme: c } = props;

  return {
    color: `${c}.60`,
    bg: `${c}.10`,
    _hover: {
      color: `${c}.70`,
      bg: `${c}.20`,
    },
    _active: {
      color: `${c}.70`,
      bg: `${c}.30`,
    },
  };
};

const variantOutline: SystemStyleFunction = (props) => {
  const { colorScheme: c } = props;
  return {
    border: '1.5px solid',
    borderColor: `${c}.60`,
    ...variantGhost(props),
  };
};

const variantSolid: SystemStyleFunction = (props) => {
  const { colorScheme: c } = props;

  return {
    bg: `${c}.60`,
    color: 'white',
    _hover: {
      bg: `${c}.70`,
      _disabled: {
        bg: `${c}.80`,
      },
    },
    _active: { bg: `${c}.90` },
  };
};

const variantLink: SystemStyleFunction = (props) => {
  const { colorScheme: c } = props;
  return {
    px: 0,
    py: 0,
    display: 'inline',
    lineHeight: 'normal',
    verticalAlign: 'baseline',
    color: `${c}.600`,
    _hover: {
      textDecoration: 'underline',
      _disabled: {
        textDecoration: 'none',
      },
    },
    _active: {
      color: `${c}.600`,
    },
  };
};

const variantUnstyled: SystemStyleObject = {
  bg: 'none',
  color: 'inherit',
  lineHeight: 'inherit',
  m: 0,
  p: 0,
};

const variants = {
  ghost: variantGhost,
  outline: variantOutline,
  solid: variantSolid,
  link: variantLink,
  unstyled: variantUnstyled,
};

const sizes: Record<string, SystemStyleObject> = {
  lg: {
    fontSize: 'md',
    px: 6,
    py: 3,
  },
  md: {
    fontSize: 'sm',
    px: 5,
    py: 2,
  },
  sm: {
    fontSize: 'sm',
    px: 2,
    py: 1,
  },
};

const defaultProps = {
  variant: 'solid',
  size: 'md',
  colorScheme: 'primary',
};

export const Button = {
  baseStyle,
  variants,
  sizes,
  defaultProps,
};
