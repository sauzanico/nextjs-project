import { tableAnatomy as parts } from '@chakra-ui/anatomy';
import type {
  PartsStyleFunction,
  PartsStyleObject,
  SystemStyleObject,
} from '@chakra-ui/theme-tools';

const baseStyle: PartsStyleObject<typeof parts> = {
  table: {
    fontVariantNumeric: 'lining-nums tabular-nums',
    borderCollapse: 'collapse',
    width: 'full',
  },
  th: {
    fontFamily: 'poppins',
    letterSpacing: 'normal',
    textTransform: 'inherit',
    fontWeight: 'semibold',
    textAlign: 'left',
  },
  td: {
    textAlign: 'start',
  },
  caption: {
    mt: 4,
    fontFamily: 'heading',
    textAlign: 'center',
    fontWeight: 'medium',
  },
};

const numericStyles: SystemStyleObject = {
  '&[data-is-numeric=true]': {
    textAlign: 'end',
  },
};

const variantSimple: PartsStyleFunction<typeof parts> = () => {
  return {
    th: {
      color: 'warmGrey.500',
      borderBottom: '1px',
      borderColor: 'coolGrey.50',
      ...numericStyles,
    },
    td: {
      borderBottom: '1px',
      borderColor: 'coolGrey.50',
      ...numericStyles,
    },
    caption: {
      color: 'warmGrey.500',
    },
    tr: {
      '&:last-of-type': {
        td: { borderBottomWidth: 0 },
      },
    },
  };
};

const variants = {
  simple: variantSimple,
  unstyled: {},
};

const sizes: Record<string, PartsStyleObject<typeof parts>> = {
  sm: {
    th: {
      px: '1',
      py: '2',
      lineHeight: '4',
      fontSize: 'xs',
    },
    td: {
      px: '1',
      py: '2',
      fontSize: 'sm',
      lineHeight: '4',
    },
    caption: {
      px: '1',
      py: '1',
      fontSize: 'xs',
    },
  },
  md: {
    th: {
      px: '2',
      py: '4',
      lineHeight: '4',
      fontSize: 'sm',
    },
    td: {
      px: '2',
      py: '4',
      lineHeight: '5',
    },
    caption: {
      px: '2',
      py: '2',
      fontSize: 'sm',
    },
  },
  lg: {
    th: {
      px: '2',
      py: '4',
      lineHeight: '4',
      fontSize: 'sm',
    },
    td: {
      px: '2',
      py: '6',
      lineHeight: '5',
    },
    caption: {
      px: '2',
      py: '4',
      fontSize: 'sm',
    },
  },
};

const defaultProps = {
  variant: 'simple',
  size: 'md',
};

export const Table = {
  parts: parts.keys,
  baseStyle,
  variants,
  sizes,
  defaultProps,
};
