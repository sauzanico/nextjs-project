import { Styles, SystemStyleFunction } from '@chakra-ui/theme-tools';

import * as externals from './externals';

const externalsStyles: SystemStyleFunction = (props) =>
  Object.values(externals).reduce(
    (acc, cur) => ({
      ...acc,
      ...(typeof cur === 'function' ? cur(props) : cur),
    }),
    {},
  );

export const styles: Styles = {
  global: (props) => ({
    html: {
      bg: 'inherit',
    },
    body: {
      bg: 'neutral.10',
      color: 'primary.60',
      fontSize: 'sm',
    },
    ...externalsStyles(props),
  }),
};
