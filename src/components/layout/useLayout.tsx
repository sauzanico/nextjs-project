import { useEffect, useState } from 'react';
import { ComponentWithAs, IconProps } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { generateBreadcrumb } from '@utils';
import {
  DashboardMenu,
  Train,
  WilayahMenu,
  PerjalananMenu,
  PerhitunganMenu,
  PengaturanMenu,
  KelolaAksesMenu,
} from '@components';

type Menu = {
  name: string;
  href: string;
  identifier: string;
  icon?: ComponentWithAs<'svg', IconProps>;
  children?: {
    name: string;
    href: string;
    identifier: string;
  }[];
};

type BreadCrumb = {
  name: string;
  href: string;
};

export function useLayout() {
  const [menus, setMenus] = useState<Menu[]>([]);
  const [breadcrumbList, setBreadcrumbList] = useState<BreadCrumb[]>([]);
  const [activeMenu, setActiveMenu] = useState<string>('');
  const [activeParentMenu, setActiveParentMenu] = useState<string>('');
  const [activeDropdownMenus, setActiveDropdownMenus] = useState<string[]>([]);
  const [showFull, setShowFull] = useState(true);
  const Router = useRouter();

  const onClickMenu = (menu: Menu) => {
    let href = menu.href;
    const currentActiveDropdownMenus = [...activeDropdownMenus];
    if (menu.children) {
      const index = currentActiveDropdownMenus.indexOf(menu.name);
      if (index > -1) {
        currentActiveDropdownMenus.splice(index, 1);
        href = Router.pathname;
      } else {
        currentActiveDropdownMenus.push(menu.name);
        href = menu.children[0].href;
      }
    }
    setActiveDropdownMenus(currentActiveDropdownMenus);
    Router.push({ pathname: href });
  };

  const defaultMenu: Menu[] = [
    {
      name: 'Dashboard',
      identifier: '',
      href: '/',
      icon: DashboardMenu,
    },
    {
      name: 'Sarana & Kereta',
      href: 'sarana-&-kereta',
      identifier: 'sarana-&-kereta',
      icon: Train,
      children: [
        {
          name: 'Data Sarana',
          identifier: 'data-sarana',
          href: '/sarana-&-kereta/data-sarana',
        },
        {
          name: 'Data Kereta Api',
          identifier: 'data-kereta',
          href: '/sarana-&-kereta/data-kereta',
        },
      ],
    },
    {
      name: 'Wilayah Kerja',
      href: 'wilayah-kerja',
      identifier: 'wilayah-kerja',
      icon: WilayahMenu,
      children: [
        {
          name: 'Stasiun',
          href: '/wilayah-kerja/stasiun',
          identifier: 'stasiun',
        },
        {
          name: 'DAOP / Divre',
          identifier: 'daop-divre',
          href: '/wilayah-kerja/daop-divre',
        },
      ],
    },
    {
      name: 'Perjalanan',
      href: '/perjalanan',
      identifier: 'perjalanan',
      icon: PerjalananMenu,
    },
    {
      name: 'Perhitungan',
      href: '/perhitungan',
      identifier: 'perhitungan',
      icon: PerhitunganMenu,
    },
    {
      name: 'Pengaturan',
      href: '/pengaturan',
      identifier: '/pengaturan',
      icon: PengaturanMenu,
      children: [
        {
          name: 'Faktor Prioritas',
          href: '/pengaturan/faktor-prioritas',
          identifier: 'faktor-prioritas',
        },
        {
          name: 'Sarana',
          href: '/pengaturan/sarana',
          identifier: 'sarana',
        },
        {
          name: 'Tipe Layanan',
          href: '/pengaturan/tipe-layanan',
          identifier: 'tipe-layanan',
        },
        {
          name: 'Jenis Layanan',
          href: '/pengaturan/jenis-layanan',
          identifier: 'jenis-layanan',
        },
      ],
    },
    {
      name: 'Kelola Akses',
      href: 'kelola-akses',
      identifier: 'kelola-akses',
      icon: KelolaAksesMenu,
      children: [
        {
          name: 'Admin',
          href: '/kelola-akses/admin',
          identifier: 'admin',
        },
        {
          name: 'Badan Usaha',
          href: '/kelola-akses/badan-usaha',
          identifier: 'badan-usaha',
        },
        {
          name: 'Pengguna',
          href: '/kelola-akses/pengguna',
          identifier: 'pengguna',
        },
      ],
    },
  ];

  const init = () => {
    const path = Router.pathname.split('/');
    if (path.length > 2) {
      const parent = defaultMenu.find((item) => item.identifier === path[1]);
      const children = parent?.children?.find(
        (item) => item.identifier === path[2],
      );
      setActiveParentMenu(parent?.name ?? '');
      setActiveMenu(children?.name ?? '');
    } else {
      const current = defaultMenu.find((item) => item.identifier === path[1]);
      setActiveParentMenu('/');
      setActiveMenu(current?.name ?? '');
    }

    const breads = generateBreadcrumb(path);
    setBreadcrumbList(breads);
  };

  useEffect(() => {
    init();
  }, [Router.pathname]);

  useEffect(() => {
    setMenus(defaultMenu);
    setActiveDropdownMenus(
      defaultMenu.filter((item) => item.children).map((item) => item.name),
    );
  }, []);

  return {
    menus,
    activeMenu,
    setActiveMenu,
    activeParentMenu,
    activeDropdownMenus,
    onClickMenu,
    breadcrumbList,
    showFull,
    setShowFull,
  };
}
