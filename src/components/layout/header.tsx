import React from 'react';
import { Text, HStack, Image } from '@chakra-ui/react';

export function Header() {
  return (
    <HStack
      w="full"
      bg="primaryGreenLight"
      justifyContent="space-between"
      py={6}
      px={20}
    >
      <HStack spacing={6}>
        <Image src="/kai-logo.png" alt="logo-kai" width="80px" />
        <Text
          width="350px"
          fontSize="sm"
          color="white"
          fontWeight="semibold"
          fontFamily="poppins"
        >
          KEMENTERIAN PERHUBUNGAN DIREKTORAT JENDERAL PERKERETAAPIAN SATUAN
          KERJA PENGEMBANGAN LALU LINTAS DAN PENINGKATAN ANGKUTAN KERETA API
        </Text>
      </HStack>
      <Text
        color="white"
        fontSize="40px"
        lineHeight="40px"
        textAlign="right"
        fontWeight="poppins"
      >
        Sistem Pemantauan Kinerja <br /> Operasi Kereta Api
      </Text>
    </HStack>
  );
}
