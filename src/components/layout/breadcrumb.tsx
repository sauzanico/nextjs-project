import React from 'react';
import { HStack, Box } from '@chakra-ui/react';
import { Link } from '@components/ui-kit';

type BreadCrumbList = {
  name: string;
  href: string;
};

type BreadCrumbProps = {
  breadcrumbList: BreadCrumbList[];
};

export function BreadCrumb(props: BreadCrumbProps) {
  const { breadcrumbList } = props;

  return (
    <HStack divider={<Box>/</Box>}>
      {breadcrumbList.map((item, index) => {
        const isLastIndex = index === breadcrumbList.length - 1;
        return (
          <Link
            fontWeight={isLastIndex ? 'semibold' : 'normal'}
            variant="unstyled"
            key={item.name}
            href={index > 0 && item.href ? item.href : {}}
          >
            {item.name}
          </Link>
        );
      })}
    </HStack>
  );
}
