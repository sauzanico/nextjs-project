import React, { ReactElement } from 'react';
import { HStack, Box } from '@chakra-ui/react';
import { SideBar } from './sidebar';
import { useLayout } from './useLayout';
import { BreadCrumb } from './breadcrumb';
// import { NextBreadcrumbs } from './test';

type LayoutProps = {
  children: ReactElement;
};

export function Layout(props: LayoutProps) {
  const {
    menus,
    activeMenu,
    activeParentMenu,
    activeDropdownMenus,
    onClickMenu,
    showFull,
    setShowFull,
    breadcrumbList,
  } = useLayout();

  const breadcrumbProps = {
    breadcrumbList,
  };

  const sideBarProps = {
    menus,
    activeDropdownMenus,
    showFull,
    setShowFull,
    activeMenu,
    activeParentMenu,
    onClickMenu,
  };

  return (
    <HStack width="full" alignItems="flex-start">
      <SideBar {...sideBarProps} />
      <Box flex={1} p={8}>
        <BreadCrumb {...breadcrumbProps} />
        {props.children}
      </Box>
    </HStack>
  );
}
