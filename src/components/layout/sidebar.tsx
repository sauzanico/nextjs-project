import React, { Dispatch, SetStateAction } from 'react';
import {
  HStack,
  VStack,
  Box,
  Text,
  ComponentWithAs,
  IconProps,
} from '@chakra-ui/react';
import {
  ArrawUp,
  ArrawDown,
  ArrawLeftDouble,
  ArrawRightDouble,
} from '@components';

type Menu = {
  name: string;
  href: string;
  identifier: string;
  icon?: ComponentWithAs<'svg', IconProps>;
  children?: {
    name: string;
    href: string;
    identifier: string;
  }[];
};

type SideBarProps = {
  menus: Menu[];
  activeMenu: string;
  activeParentMenu: string;
  activeDropdownMenus: string[];
  onClickMenu: (menu: Menu) => void;
  showFull: boolean;
  setShowFull: Dispatch<SetStateAction<boolean>>;
};

export function SideBar(props: SideBarProps) {
  const {
    menus,
    activeMenu,
    activeParentMenu,
    activeDropdownMenus,
    onClickMenu,
    showFull,
    setShowFull,
  } = props;
  return (
    <Box h="100vh" overflowY="auto" bg="neutral.10" px={4} shadow="lg">
      <HStack
        justifyContent="center"
        w="full"
        onClick={() => setShowFull(!showFull)}
        mt={8}
        mb={8}
        cursor="pointer"
      >
        {showFull ? (
          <>
            <ArrawLeftDouble boxSize={3} />
            <Text>Sembunyikan Menu</Text>
          </>
        ) : (
          <>
            <ArrawRightDouble boxSize={3} />
          </>
        )}
      </HStack>
      <VStack width="full">
        {menus.map((menu) => {
          const MenuIcon = menu.icon;
          const isActiveDropdownMenu = activeDropdownMenus.includes(menu.name);
          const isActiveMenu =
            activeMenu === menu.name || activeParentMenu === menu.name;
          return (
            <Box key={menu.name} w="full">
              <HStack
                justifyContent="flex-start"
                alignItems="center"
                spacing={6}
                width="full"
                px={4}
                py={2}
                borderRadius="3xl"
                cursor="pointer"
                _hover={
                  !isActiveMenu
                    ? {
                        bg: 'primary.10',
                        color: 'primary.60',
                      }
                    : {}
                }
                onClick={() => onClickMenu(menu)}
                {...(isActiveMenu
                  ? {
                      bg: 'primary.60',
                      color: 'neutral.10',
                    }
                  : {
                      bg: 'transparent',
                      color: 'primary.60',
                    })}
              >
                {MenuIcon && <MenuIcon boxSize={5} />}
                {showFull && (
                  <>
                    <Text>{menu.name}</Text>
                    {menu.children && (
                      <Box>
                        {isActiveDropdownMenu ? (
                          <ArrawUp onClick={console.log} boxSize={3} />
                        ) : (
                          <ArrawDown onClick={console.log} boxSize={3} />
                        )}
                      </Box>
                    )}
                  </>
                )}
              </HStack>
              {showFull &&
                isActiveDropdownMenu &&
                menu.children &&
                menu.children.map((item) => {
                  return (
                    <HStack
                      spacing={6}
                      key={item.name}
                      px={4}
                      py={2}
                      onClick={() => onClickMenu(item)}
                      _hover={{
                        bg: 'primary.10',
                        color: 'primary.60',
                      }}
                      cursor="pointer"
                    >
                      <Box boxSize={5}></Box>
                      <Text
                        {...(activeMenu === item.name && {
                          fontWeight: 'bold',
                        })}
                      >
                        {item.name}
                      </Text>
                    </HStack>
                  );
                })}
            </Box>
          );
        })}
      </VStack>
    </Box>
  );
}
